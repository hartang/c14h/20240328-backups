# Meine selbstgebastelte Backup-Lösung

Backups sollte wohl jeder in irgendeiner Form haben und man findet im Internet
auch viel Software dazu. Nur irgendwie habe ich noch nicht die Backup-Software
gefunden, die alle meine Wünsche befriedigt. Daher habe ich mir prompt selbst
was gebastelt - und das möchte ich hier einmal vorstellen.

## Ausführen

Die Präsentation wird in einem Container ausgeführt. Dazu benötigt man `podman`
oder `docker` auf dem Rechner. Der folgende Befehl lädt dann den Container
herunter oder baut ihn lokal und startet die Präsentation:

```
$ ./present.sh
```
