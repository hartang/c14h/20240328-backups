---
title: Meine selbstgebastelte Backup-Lösung
author: Andreas Hartmann (@hartan)
patat:
    breadcrumbs: true
    wrap: true
    slideLevel: 2
    margins:
        left: 5
        right: 5
        top: 1
        bottom: 1
...

# Meine selbstgebastelte Backup-Lösung


## Backups früher

- Hm...


## Backups vor ca. 2 Jahren

- Bestandteile:
    - `btrfs` CLI Tools (`btrfs-progs`)
    - USB-Festplatte mit Btrfs
- Lief nur auf meinem Laptop

. . .

- Irgendwann später auch auf meinem Desktop
- *Aber halt alles sehr manuell...*


## Backups vor ca. 1.5 Jahren

- Erkenntnis:
    - Backups mit "builtin"-tools sind cool
    - Btrfs ist mir eigentlich ganz sympathisch
    - Aber zentraler Speicher wäre toll

. . .

- Rechner machen regelmäßig Snapshots (alle 15 Minuten)
    - Siehe [frühere c14h][c14h]
- NAS neu eingerichtet mit Fedora CoreOS
    - Speicher läuft mit Btrfs
    - Rechner pushen Subvolumes auf NAS
    - **Aber**: Separate Subvolumes, nur zum Backup erstellt


## Backups vor ca. 1 Jahr

- Erkenntnis:
    - NAS sollte Backups von Rechnern ziehen

. . .

- *Idee*: Alle Backups per Btrfs!
- **Problem**:
    - Daheim läuft ein Raspi, der hat kein Btrfs...
    - Damit ist eine Komplettlösung mit [btbrk][btrbk] nicht möglich

. . .

- Bash ftw!


## Warum Bash?

- Universell:
    - Bash ist eigentlich überall vorhanden, kein externes Tooling
    - Fedora CoreOS ist ein Minimalsystem zum Container hosten
- Ad-hoc erweiterbar
- Ideal geeignet, um Systembefehle auszuführen
    - Kann bestehende Einzelkomponenten kombinieren


## Backups seit ca. 4 Monaten

- Erkenntnis:
    - Snapshots nur zu "Backup-Terminen" sind doof

. . .

- Idee: *Recycle meine bestehenden Snapshots*
- Rechner behalten unendlich viele daily-snapshots
- NAS zieht sich alle neuen daily-snapshots
- **Nach** dem Backup werden alte Snapshots auf Rechnern gelöscht
    


[btrbk]: https://github.com/digint/btrbk
[c14h]: https://www.noname-ev.de/chaotische_viertelstunde.html?latest=611#c14h_603
